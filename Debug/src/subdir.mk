################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ProgDyn.c \
../src/find_by_dichotomy.c \
../src/matrice.c \
../src/sacAdos.c 

OBJS += \
./src/ProgDyn.o \
./src/find_by_dichotomy.o \
./src/matrice.o \
./src/sacAdos.o 

C_DEPS += \
./src/ProgDyn.d \
./src/find_by_dichotomy.d \
./src/matrice.d \
./src/sacAdos.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


