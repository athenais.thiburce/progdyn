#include "matrice.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



/**
 * \file   matrice.c
 * \author Athenais Thiburce
 * \version 0.1
 *
 */


/**
 * Remplissage de matrice avec des ' ' et *
 * \param tab The array of values
 * \param ligne ligne du tableau
 * \param colonne colonne du tableau
 *
 */
void remplissage_matrice(int *tab,int ligne,int colonne){
	char array[ligne][colonne];

	for(int i=0;i<=ligne;i++){
		for(int j=0;j<colonne;j++){
			if(*(tab + (i*colonne+j)) == 1){

				array[i][j]='*';
			}
			else{
				array[i][j]=' ';
			}
		}

	}
	for(int k=0;k<ligne;k++){
		for(int l=0;l<colonne;l++){
			printf("%c", array[k][l]);
		}
		printf("\n");
	}
}


/**
 * Trouver le plus grand carre vide
 * \param tab The array of values
 * \param size taille du tableau
 *
 */
void find_carre(int tab, int size) {
	PGCB pg;
	PGCB pgmax;

	pgmax->x = 0;
	pgmax->y = 0;
	pgmax->taille = 0;

	for (int y = 0; y < size; y++) {
		for (int x = 0; x < size; x++) {

			if ((tab + (y * x + x)) == 0) {

				pg->x = x;
				pg->y = y;
				pg->taille = 1;
				bool flag = true;

				while (*(tab[pg->x + pg->taille][pg->y]) == 0 && tab[pg->x][pg->y + pg->taille] == 0 && flag == true) {

					for (int k = pg->y; k < (pg->y + pg->taille) && k < size - 1; k++) {
						for (int l = pg->x; 1 < (pg->x + pg->taille) && l < size - 1; l++) {
							if (pg->y + pg->taille == pg->x + pg->taille) {
								pg->taille = pg->taille + 1;
								if (tab[l][k] != 0) {
									flag = false;
									pg->taille = pg->taille - 1;
								}
							}
						}
					}

				}

				if (pg->taille > pgmax->taille) {
					pgmax->x = pg->x;
					pgmax->y = pg->y;
					pgmax->taille = pg->taille;
				}


			}



		}
	}

}

void affichage_carre(){

}

/**
 * COnstruction et affichage de la matrice binaire
 * \param ligne ligne du tableau
 * \param colonne colonne du tableau
 *
 */
void construir_matrice(int ligne,int colonne){

	int arr[ligne][colonne];
	int size =ligne*colonne;

	for(int i=0;i<=ligne;i++){
		for(int j=0;j<colonne;j++){
			arr[i][j]=rand() % 2;
		}

	}
	///partie affichage de la matrice
	for(int k=0;k<ligne;k++){
		for(int l=0;l<colonne;l++){
			printf("%i", arr[k][l]);  //%i pout int
		}
		printf("\n");
	}
	remplissage_matrice(arr,ligne,colonne);
	find_carre(arr,size);

}



