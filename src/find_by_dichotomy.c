#include "find_by_dichtomy.h"


/**
 * \file   find_by_dichtomy.c
 * \author Athenais Thiburce
 * \version 0.1
 *
 */


/**
 * Recherche par dichotomie dans un tableau d'entiers
 * \param array The array of values
 * \param size_t The size of the array
 * \param value The value to find
 * \return The position of the value found or -1
 */
int find_by_dichotomy(int array[], int size_t, int value ){
	int l=array[0]; //Debut tableau
	int r=size_t-1; //fin tableau
	while (l <= r) {
		int m = l + (r - l) / 2; //le milieu du tableau

		// On regarde si x est au milieu du tableau
		if (array[m] == value)
			return m;

		// si x est plus grand, on ignore la partie gauche du tabelau
		if (array[m] < value)
			l = m + 1;

		// si x est plus petit on ignore la partie droite du tabelau
		else
			r = m - 1;
	}


	// si on arrive la c'est que pas dans tableau
	return -1;

}

