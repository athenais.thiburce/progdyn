#include "sacAdos.h"

/**
 * \file   sacAdos.c
 * \author Athenais Thiburce
 * \version 0.1
 *
 */

/**
 * Trouver la valeur plus grande
 *\param a values
 *\param b vlues
 *
 */
int max(int a, int b) { //mise la car on l'a reuse pas

	return (a > b) ? a : b;
}

/**
 * Trouver la valeur plus grande
 *\param w capacite du sac
 *\param poid poid
 *\param cout cout
 *\param n taille
 *
 */
// Renvoie la valeur maximale qu'on peut être mettre dans un sac à dos de capacité W
int knapSack(int W, int poid[], int cout[], int n)  //wt tableau pod; val tableau cout
{
    int i, w;
    int K[n + 1][W + 1];

    // faire table K[][] de manière ascendante
    for (i = 0; i <= n; i++) {
        for (w = 0; w <= W; w++) {
            if (i == 0 || w == 0)
                K[i][w] = 0;
            else if (poid[i - 1] <= w)
                K[i][w] = max(
                		cout[i - 1] + K[i - 1][w - poid[i - 1]],
                    K[i - 1][w]);
            else
                K[i][w] = K[i - 1][w];
        }
    }

    return K[n][W];
}
