/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   find_by_dichtomy.h
 * Author: Athenais
 *
 */

#ifndef DICHTOMY_H
#define DICHTOMY_H
#define DICHTOMY_MAX_SIZE 100

typedef struct {
    float data[DICHTOMY_MAX_SIZE];
    int index;
} Dichtomy;

#endif

int find_by_dichotomy(int array[], int size_t, int value );

