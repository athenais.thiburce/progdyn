#ifndef MATRICE_H
#define MATRICE_H

#define MATRICE_MAX_SIZE 100

typedef struct {
    int x;
    int y;
    int taille;
} PGCB;

#endif

void construir_matrice(int colonne,int ligne);
void remplissage_matrice(int *tab,int ligne,int colonne);
void find_carre(int tab, int size);



