/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   find_by_dichtomy.h
 * Author: Athenais
 *
 */

#ifndef SAC_H
#define SAC_H

#define SAC_MAX_SIZE 100

typedef struct {
    float data[SAC_MAX_SIZE];
    int index;
} Sac;

#endif

int max(int a, int b);
int knapSack(int W, int wt[], int val[], int n);

